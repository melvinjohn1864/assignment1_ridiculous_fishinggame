//
//  GameScene.swift
//  Assignment_Ridiculous_Fishing_Clone
//
//  Created by Melvin John on 2019-10-19.
//  Copyright © 2019 Melvin John. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var gameBackground = SKSpriteNode()
    var hook = SKSpriteNode()
    var checkGameStarted = false
    var spawnedFishes:[SKSpriteNode] = []
    var hookedFishes:[SKSpriteNode] = []
    var score = SKLabelNode()
    var hookDistance = SKLabelNode()
    var restartGameLabel = SKLabelNode()
    var distanceMoved = 0
    var isAddFishes = true
    var isBackgroundReverse = false
    var isMethodExecuted = false
    var isHookTouchedSurface = false
    var isHookMovedDown = false
    var isFishRemovedAtTouch = false
    var isDisperseDone = false
    var regulateSpawnFish = 0
    
    override func didMove(to view: SKView) {
        self.gameBackground = self.childNode(withName: "gameBackground") as! SKSpriteNode
        self.hook = self.childNode(withName: "hook") as! SKSpriteNode
        //self.hookDistance = self.childNode(withName: "hookDistance") as! SKLabelNode
        self.score = self.childNode(withName: "score") as! SKLabelNode
    }
    
    override func update(_ currentTime: TimeInterval) {
        regulateSpawnFish = regulateSpawnFish + 1
        if checkGameStarted {
            if isAddFishes {
                if regulateSpawnFish % 30 == 0  {
                    spawnFishes()
                    spawnLeftFishes()
                }
            }
            if !isBackgroundReverse {
                self.moveBgUp()
            }
            for(i, fish) in self.spawnedFishes.enumerated(){
                if(self.hook.frame.intersects(fish.frame) == true){
                    self.hookedFishes.append(fish)
                    isBackgroundReverse = true
                    if !(self.distanceMoved == 0) {
                        self.moveBgDown()
                    }else {
                        self.removeNScatterFishes()
                    }
                    let attachToHook = SKAction.move(to: self.hook.position, duration: 0.1)
                    fish.run(attachToHook)
                    fish.zRotation = CGFloat(-90)
                    if !self.isHookTouchedSurface{
                        print("Caught Fish")
                        fish.removeFromParent()
                        self.spawnedFishes.remove(at: i)
                        addChild(fish)
                    }
                }
            }
        }
    }
    
    func spawnFishes(){
        let XPos = CGFloat(300)
        let YPos = CGFloat.random(in: -size.height ... 0)
        let fish = SKSpriteNode(imageNamed: makeLeftRandomFish())
        fish.size.width = 100
        fish.size.height = 100
        fish.position = CGPoint(x: XPos, y: YPos)
        fish.zPosition = CGFloat(2)
        addChild(fish)
        self.spawnedFishes.append(fish)
        regulateFishDirection(sprite: fish)
    }
    
    func spawnLeftFishes(){
        let XPos = CGFloat(-300)
        let YPos = CGFloat.random(in: -size.height ... 0)
        let fish = SKSpriteNode(imageNamed: makeRightRandomFish())
        fish.size.width = 100
        fish.size.height = 100
        fish.position = CGPoint(x: XPos, y: YPos)
        fish.zPosition = CGFloat(2)
        addChild(fish)
        self.spawnedFishes.append(fish)
        regulateLeftFishDirection(sprite: fish)
    }
    
    func makeLeftRandomFish()->String{
        let fishes: [String] = ["fish", "yellowFish", "orangeFish", "divingFish"]
        return fishes[Int.random(in: 0 ..< fishes.count)]
    }
    
    func makeRightRandomFish()->String{
        let fishes: [String] = ["leftFish", "shark"]
        return fishes[Int.random(in: 0 ..< fishes.count)]
    }
    
    func randomYPos()->CGFloat{
        return CGFloat.random(in: 300 ... 600)
    }
    
    func randomDisperseXPosition()->CGFloat{
        return CGFloat.random(in: -320 ... 100)
    }
    
    func randomDisperseYPosition()->CGFloat{
        return CGFloat.random(in: 320 ... 900)
    }
    
    func disperseFishes(){
        print("Inside scatter fishes")
        for(_, fish) in self.hookedFishes.enumerated(){
            let scatterAction = SKAction.move(to: CGPoint(x: randomDisperseXPosition(), y: randomDisperseYPosition()), duration: 3)
            fish.run(scatterAction)
            self.isDisperseDone = true
        }
    }
    
    func changeHookPostion(touches: Set<UITouch>) {
        let touch = touches.first
        if (touch == nil) {
            return
        }
        // print("Inside touchesbegan")
        let touchLocation = touch!.location(in: self)
        if (touchLocation.x < 0) {
            //print("Inside left arrow")
            self.hook.position.x = self.hook.position.x - 20
        }
        else if (touchLocation.x > 0) {
            //print("Inside right arrow")
            self.hook.position.x = self.hook.position.x + 20
        }
    }
    
    func moveBgUp(){
        self.distanceMoved = self.distanceMoved + 2
        self.score.text = String(self.distanceMoved)
       // self.hookDistance.text = String(self.distanceMoved)
        let moveBackground = SKAction.move(to: CGPoint(x:0, y: gameBackground.size.height - UIScreen.main.nativeBounds.height), duration: 10)
        self.gameBackground.run(moveBackground)
    }
    
    func moveBgDown(){
        self.isHookTouchedSurface = true
        self.distanceMoved = self.distanceMoved - 2
        self.hookDistance.text = String(self.distanceMoved)
        let reverseBackground = SKAction.move(to: CGPoint(x:0, y: UIScreen.main.nativeBounds.height - gameBackground.size.height), duration: 5)
        self.gameBackground.run(reverseBackground)
        
    }
    
    func removeNScatterFishes(){
        self.spawnedFishes.removeLast()
        self.gameBackground.removeAllActions()
        self.hook.removeFromParent()
        if !self.isMethodExecuted{
            self.disperseFishes()
            self.isMethodExecuted = true
        }
        self.isAddFishes = false
    }
    
    func regulateFishDirection(sprite: SKSpriteNode){
        if !self.isBackgroundReverse {
            let moveX = SKAction.moveBy(x: -size.width , y: 600, duration: 10)
            sprite.run(moveX)
        }else if self.isBackgroundReverse {
            let moveX = SKAction.moveBy(x: -size.width , y: -200, duration: 20)
            sprite.run(moveX)
        }
    }
    
    func regulateLeftFishDirection(sprite: SKSpriteNode){
        if !self.isBackgroundReverse {
            let moveX = SKAction.moveBy(x: size.width , y: 600, duration: 10)
            sprite.run(moveX)
        }else if self.isBackgroundReverse {
            let moveX = SKAction.moveBy(x: size.width , y: -200, duration: 20)
            sprite.run(moveX)
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        if (touch == nil) {
            return
        }
        let touchLocation = touch!.location(in: self)
        self.checkGameStarted = true
        self.changeHookPostion(touches: touches)
        if !isFishRemovedAtTouch && self.isDisperseDone{
            var count = 0
            for(_, fish) in self.hookedFishes.enumerated(){
                
                if fish.contains(touchLocation){
                    fish.removeFromParent()
                    count = count + 1
                }
            }
            self.distanceMoved = self.distanceMoved +  count * 5
            self.score.text = String(self.distanceMoved)
        }
        
    }
    
    
}
